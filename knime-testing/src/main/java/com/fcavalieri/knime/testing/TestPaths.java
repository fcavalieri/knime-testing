package com.fcavalieri.knime.testing;

import java.nio.file.Path;
import java.util.Arrays;

public class TestPaths {
    public static Path baseFolder() {
        return baseFolder(Path.of(System.getProperty("user.dir")).toAbsolutePath());
    }

    public static Path projectFolder() {
        return projectFolder(Path.of(System.getProperty("user.dir")).toAbsolutePath());
    }

    public static Path targetFolder() {
        return projectFolder().resolve("target");
    }

    public static Path p2RepositoryFolder() {
        return baseFolder().resolve("p2/target/repository");
    }

    private static Path baseFolder(Path currentFolder) {
        do {
            if (Arrays.stream(currentFolder.toFile().listFiles()).anyMatch(f -> f.isDirectory() && f.getName().equals(".git")))
                return currentFolder;
            currentFolder = currentFolder.getParent();
        } while (!currentFolder.equals(Path.of("/")));
        throw new RuntimeException("Cannot identify base folder");
    }

    private static Path projectFolder(Path currentFolder) {
        do {
            if (Arrays.stream(currentFolder.toFile().listFiles()).anyMatch(f -> f.isFile() && (f.getName().equals("pom.xml") || f.getName().equals("build.gradle"))))
                return currentFolder;
            currentFolder = currentFolder.getParent();
        } while (!currentFolder.equals(Path.of("/")));
        throw new RuntimeException("Cannot identify project folder");
    }

    public static Path cacheDir() {
        return baseFolder().resolve("cache");
    }
}
