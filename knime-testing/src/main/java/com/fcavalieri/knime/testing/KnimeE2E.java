package com.fcavalieri.knime.testing;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.maven.plugin.MojoExecutionException;

import de.philippkatz.maven.plugins.TestParserMojo;

public class KnimeE2E {
    private static final String KNIME_URL_PATTERN_LINUX = "https://download.knime.org/analytics-platform/linux/knime_%s.linux.gtk.x86_64.tar.gz";
    private static final String KNIME_LOCAL_PATTERN_LINUX = "knime_%s.linux.gtk.x86_64.tar.gz";
    private static final String EXECUTABLE_NAME_LINUX = "knime";

    private static final String KNIME_URL_PATTERN_WINDOWS = "https://download.knime.org/analytics-platform/win/knime_%s.win32.win32.x86_64.zip";
    private static final String KNIME_LOCAL_PATTERN_WINDOWS = "knime_%s.win32.win32.x86_64.zip";
    private static final String EXECUTABLE_NAME_WINDOWS = "knime.exe";

    private static final String KNIME_FOLDER_PATTERN = "knime_%s";
    private static final String KNIME_REPOSITORY_PATTERN = "https://update.knime.com/analytics-platform/%s/";

    private final String knimeURLPattern;
    private final String knimeLocalPattern;
    private final String knimeExecutable;
    private final boolean reuseInstallation;

    public static class Plugin {
        private final String id;
        private final String repository;

        public Plugin(String id) {
            this(id, null);
        }

        public Plugin(String id, String repository) {
            this.id = id;
            this.repository = repository;
        }
    }

    public KnimeE2E(boolean reuseInstallation) {
        if (SystemUtils.IS_OS_LINUX) {
            knimeURLPattern = KNIME_URL_PATTERN_LINUX;
            knimeLocalPattern = KNIME_LOCAL_PATTERN_LINUX;
            knimeExecutable = EXECUTABLE_NAME_LINUX;
        }
        else if (SystemUtils.IS_OS_WINDOWS) {
            knimeURLPattern = KNIME_URL_PATTERN_WINDOWS;
            knimeLocalPattern = KNIME_LOCAL_PATTERN_WINDOWS;
            knimeExecutable = EXECUTABLE_NAME_WINDOWS;
        }
        else {
            throw new RuntimeException("Only Linux and Windows are supported, using: <" + SystemUtils.OS_NAME + ">");
        }

        this.reuseInstallation = reuseInstallation;
    }

    private void cacheKnimeInstaller(String version) throws IOException {
        System.out.println("Verifying version " + version);
        URL url = new URL(String.format(knimeURLPattern, version));
        Path localFile = TestPaths.cacheDir().resolve(String.format(knimeLocalPattern, version));
        if (Files.exists(localFile)) {
            System.out.println("Version " + version + " is already present in cache");
            try {
                if (SystemUtils.IS_OS_LINUX) {
                    TarArchiveInputStream fin = new TarArchiveInputStream(new GzipCompressorInputStream(new FileInputStream(localFile.toFile())));
                    while (fin.getNextTarEntry() != null);
                    return;
                } else if (SystemUtils.IS_OS_WINDOWS) {
                    ZipArchiveInputStream fin = new ZipArchiveInputStream(new FileInputStream(localFile.toFile()));
                    while (fin.getNextZipEntry() != null);
                    return;
                } else {
                    throw new RuntimeException("Only Linux and Windows are supported, using: <" + SystemUtils.OS_NAME + ">");
                }
            } catch (IOException e) {
                System.out.println("Cached Knime version " + version + " is corrupt");
                Files.delete(localFile);
            }
        }
        System.out.println("Downloading version " + version);
        FileUtils.copyURLToFile(url, localFile.toFile());
    }

    private void extractKnimeInstaller(String version, boolean reuseInstallation) throws IOException, InterruptedException {
        System.out.println("Extracting version " + version);
        Path installerFile = TestPaths.cacheDir().resolve(String.format(knimeLocalPattern, version));
        Path installationFolder = TestPaths.targetFolder().resolve("knime-installations").resolve(String.format(KNIME_FOLDER_PATTERN, version));
        Path extractionFolder = Files.createTempDirectory("knime-tests");
        if (installationFolder.toFile().exists()) {
            if (reuseInstallation) {
                System.out.println("Version " + version + " is already extracted");
                return;
            } else {
                System.out.println("Clearing version " + version);
                FileUtils.deleteDirectory(installationFolder.toFile());
            }
        }

        Files.createDirectories(extractionFolder);
        ProcessBuilder builder = new ProcessBuilder();
        builder.command("tar", "xfz", installerFile.toAbsolutePath().toString());
        builder.directory(extractionFolder.toFile());
        Process process = builder.start();
        StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), System.out::println);
        Executors.newSingleThreadExecutor().submit(streamGobbler);
        int exitCode = process.waitFor();
        if (exitCode != 0) {
            throw new IOException("Cannot extract Knime " + version);
        }
        if (!Files.isDirectory(installationFolder.getParent()))
            Files.createDirectories(installationFolder.getParent());
        FileUtils.moveDirectory(extractionFolder.resolve(String.format(KNIME_FOLDER_PATTERN, version)).toFile(), installationFolder.toFile());
    }

    private void installLocalPlugin(String version, Plugin plugin, boolean reuseInstallation) throws IOException, InterruptedException {
        String p2RepositoryUri = "file://" + TestPaths.p2RepositoryFolder().toAbsolutePath();

        Path installationFolder = TestPaths.targetFolder().resolve("knime-installations").resolve(String.format(KNIME_FOLDER_PATTERN, version)).toAbsolutePath();
        Path executablePath = installationFolder.resolve(knimeExecutable);
        System.out.println("Verifying plugin " + plugin + " in Knime version " + version);
        if (reuseInstallation && isInstalled(plugin.id, executablePath)) {
            System.out.println("Local plugin " + plugin + " already installed in Knime version " + version + ", it will be removed and reinstalled");
            doUninstall(plugin.id, executablePath);
        }
        System.out.println("Installing plugin " + plugin + " to Knime version " + version);
        doInstall(plugin.id, plugin.repository != null ? plugin.repository : p2RepositoryUri, executablePath);
    }

    private void installPublicPlugin(String version, Plugin plugin) throws IOException, InterruptedException {
        String knimeRepository = String.format(KNIME_REPOSITORY_PATTERN, version.substring(0, version.lastIndexOf(".")));

        Path installationFolder = TestPaths.targetFolder().resolve("knime-installations").resolve(String.format(KNIME_FOLDER_PATTERN, version)).toAbsolutePath();
        Path executablePath = installationFolder.resolve(knimeExecutable);
        System.out.println("Verifying plugin " + plugin + " in Knime version " + version);
//        if (reuseInstallation && isInstalled(plugin.id, executablePath)) {
//            System.out.println("Public plugin " + plugin + " already installed in Knime version " + version);
//            return;
//        }
        System.out.println("Installing plugin " + plugin + " to Knime version " + version);
        doInstall(plugin.id, plugin.repository != null ? plugin.repository : knimeRepository, executablePath);
    }

    private boolean isInstalled(String plugin, Path executablePath) throws IOException, InterruptedException {
        //Surefire breaks standard methods of capturing a process output
        ProcessBuilder builder = new ProcessBuilder();

        builder.command(List.of(
                executablePath.toAbsolutePath().toString(),
                "-nosplash",
                "-consolelog",
                "-application", "org.eclipse.equinox.p2.director",
                "-listInstalledRoots"
        ));
        builder.directory(executablePath.getParent().toFile());

        Process process = builder.start();
        int exitCode = process.waitFor();
        if (exitCode != 0) {
            throw new IOException("Cannot list installed plugins");
        }
        String output = IOUtils.toString(process.getInputStream(), StandardCharsets.UTF_8);
        String[] lines = output.split("\\r?\\n");
        List<String> installedPlugins = Arrays.stream(lines).filter(line -> line.contains("/")).map(line -> line.split("/")[0]).collect(Collectors.toList());
        return installedPlugins.contains(plugin);
    }

    private static void doInstall(String plugin, String repository, Path executablePath) throws IOException, InterruptedException {
        ProcessBuilder builder = new ProcessBuilder();

        builder.command(List.of(
                executablePath.toAbsolutePath().toString(),
                "-nosplash",
                "-consolelog",
                "-application", "org.eclipse.equinox.p2.director",
                "-repository", repository,
                "-destination", executablePath.getParent().toString(),
                "-installIU", plugin,
                "-profile", "KNIMEProfile",
                "-profileProperties", "org.eclipse.update.install.features=true"
        ));
        builder.directory(executablePath.getParent().toFile());

        Process process = builder.start();
        StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), System.out::println);
        Executors.newSingleThreadExecutor().submit(streamGobbler);
        int exitCode = process.waitFor();
        if (exitCode != 0) {
            throw new IOException("Cannot install plugin");
        }
    }

    private static void doUninstall(String plugin, Path executablePath) throws IOException, InterruptedException {
        ProcessBuilder builder = new ProcessBuilder();

        builder.command(List.of(
                executablePath.toAbsolutePath().toString(),
                "-nosplash",
                "-consolelog",
                "-application", "org.eclipse.equinox.p2.director",
                "-uninstallIU", plugin
        ));
        builder.directory(executablePath.getParent().toFile());

        Process process = builder.start();
        StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), System.out::println);
        Executors.newSingleThreadExecutor().submit(streamGobbler);
        int exitCode = process.waitFor();
        if (exitCode != 0) {
            throw new IOException("Cannot install plugin");
        }
    }

    private static class StreamGobbler implements Runnable {
        private InputStream inputStream;
        private Consumer<String> consumer;

        public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
            this.inputStream = inputStream;
            this.consumer = consumer;
        }

        @Override
        public void run() {
            new BufferedReader(new InputStreamReader(inputStream)).lines()
                    .forEach(consumer);
        }
    }

    public void runWorkflow(String knimeVersion, List<Plugin> localPlugins, List<Plugin> remotePlugins, List<Path> workflowPaths) throws IOException, InterruptedException {
        cacheKnimeInstaller(knimeVersion);
        extractKnimeInstaller(knimeVersion, reuseInstallation);
        installPublicPlugin(knimeVersion, new Plugin("org.knime.features.testing.application.feature.group"));

        for (Plugin localPlugin: localPlugins) {
            installLocalPlugin(knimeVersion, localPlugin, reuseInstallation);
        }
        for (Plugin remotePlugin: remotePlugins) {
            installPublicPlugin(knimeVersion, remotePlugin);
        }

        for (Path workflowPath: workflowPaths) {
            Path resultPath = Path.of("target/test-results").resolve(knimeVersion).resolve(workflowPath.getFileName());
            if (resultPath.toFile().exists()) {
                FileUtils.deleteDirectory(resultPath.toFile());
            }
            Files.createDirectories(resultPath);

            //Surefire breaks standard method of capturing a process output
            Path outPath = resultPath.resolve("knime.out");
            Path errPath = resultPath.resolve("knime.err");
            System.out.println("Running workflow " + workflowPath + " in version " + knimeVersion);
            Path installationFolder = TestPaths.targetFolder().resolve("knime-installations").resolve(String.format(KNIME_FOLDER_PATTERN, knimeVersion)).toAbsolutePath();
            Path executablePath = installationFolder.resolve(knimeExecutable);
            ProcessBuilder builder = new ProcessBuilder();
            List<String> command = new ArrayList<>(needsXVFB() ? List.of("xvfb-run", executablePath.toAbsolutePath().toString()) : List.of(executablePath.toAbsolutePath().toString()));
            command.addAll(List.of(
                    "-nosplash",
                    "-consolelog",
                    "-application", "org.knime.testing.NGTestflowRunner",
                    "-root", workflowPath.toAbsolutePath().toString(),
                    "-xmlResultDir", resultPath.toAbsolutePath().toString()
            ));
            builder.command(command);
            builder.directory(installationFolder.toFile());
            builder.redirectError(errPath.toFile());
            builder.redirectOutput(outPath.toFile());
            Process process = builder.start();
            int exitCode = process.waitFor();
            System.out.println(Files.readString(errPath));
            System.out.println(Files.readString(outPath));
            if (exitCode != 0) {
                throw new IOException("Workflow " + workflowPath + " in version " + knimeVersion + " FAILED");
            }

            try {
                TestParserMojo mojo = new TestParserMojo();
                Field resultsDirectoryField = mojo.getClass().getDeclaredField("resultsDirectory");
                resultsDirectoryField.setAccessible(true);
                resultsDirectoryField.set(mojo, resultPath.toAbsolutePath().toFile());
                mojo.execute();
            } catch (MojoExecutionException | NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
                throw new RuntimeException("Test result validation failed.", e);
            }
        }
    }

    private static boolean needsXVFB() {
        boolean onCI = System.getenv("CI") != null && System.getenv("CI").compareToIgnoreCase("true") == 0;
        System.out.println((onCI ? "R" : "NOT r") + "unning on CI");
        boolean onWSL = SystemUtils.OS_VERSION.toLowerCase().contains("microsoft") && SystemUtils.OS_VERSION.toLowerCase().contains("wsl");
        System.out.println((onWSL ? "R" : "NOT r") + "unning on WSL");
        return onCI || onWSL;
    }
}
